# ExtDocr#

This sample tool is for extracting external triggers and binds on a js file, and compiling them onto an .md file.

I currently use this to list all my External calls to C++/C# for easier documentation.

### How to run ###

just type this on your console:

```
#!javascript
node generate.js path/to/your/jsfile.js

```

*I have no plans on putting this to npm since the use case of this tool is very small. The format of the external calls expected here are, as of now, limited to the company I work for*