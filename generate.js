/* globals require, process, console */

/**
 * External calls documentor
 * -------------------------
 *
 * Usage: node parse.js path/to/file.js
 * Output: currentfolder/output/{filename}.md
 *
 **/

var fs = require("fs"),
	fParser = require("./utils/fileparser.js"),
	args = process.argv.slice(2),
	os = process.platform,
	sliceDelimeter = "/";

// Stop the program if path is not defined
if (args[0] === undefined) {
	throw {
		name : "Command error",
		message : "Please specify filepath. Ex: node generate.js path/to/file.js"
	};
}

// If os is windows, we'll use \\ for the paths
if (os.indexOf("win") >= 0) {
	sliceDelimeter = "\\";
}

var filePath = args[0].split(sliceDelimeter).slice(0, -1).join(sliceDelimeter),
	fileName = args[0].split(sliceDelimeter).slice(-1);

fs.readFile(filePath + sliceDelimeter + fileName, "utf8", function (err, data) {
	if (err) throw err;

	var parsedFile = fParser.parseFile(data),
		mdFile = fParser.createMarkDown();

	fs.writeFile("./output/" + fileName + ".md", mdFile, function(err) {
		if (err) throw err;
		console.log("Document Generated!");
	});
});