# Triggers (JS_to_C)

<table>
		<tr>
		<td>get-socials</td>
		<td></td>
	</tr>
	<tr>
		<td>close-button-clicked</td>
		<td></td>
	</tr>
	<tr>
		<td>authorize-button-clicked</td>
		<td>{ service : "google" }</td>
	</tr>
	<tr>
		<td>share-button-clicked</td>
		<td>message</td>
	</tr>
</table>

# Binds (C_to_JS)

<table>
	<tr>
		<td>set-stream-values</td>
		<td></td>
	</tr>
	<tr>
		<td>close-dialog</td>
		<td></td>
	</tr>
	<tr>
		<td>set-authentication</td>
		<td></td>
	</tr>
	<tr>
		<td>show-processing</td>
		<td></td>
	</tr>
</table>

