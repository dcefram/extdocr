/* globals exports, text, console */

var FileParser = (function () {
	var FileParser = function () {
		this.data = {};
	};
	
	/**
	 * Search Array of objects for a particular needle or needle and value
	 * @param   {Array}   arr    Array of objects
	 * @param   {String}  needle Key to search
	 * @param   {String}  value  Value to search
	 * @returns {Boolean} Return true if search is successful, else, return false
	 */
	var objectSearch = function(arr, needle, value) {
		for (var key in arr) {
			if ((arr[key].name === needle && value === undefined) ||
			   (arr[key].name === needle && arr[key].params === value)) {
				return true;
			}
		}
		
		return false;
	};

	FileParser.prototype = {
		/**
		 * Fetch External triggers on the document
		 * @param   {String} text Text contents of the javascript file to parse
		 * @returns {Array}  Array of objects. [{name:"", params:""}]
		 */
		fetchTriggers: function (text) {
			/**************************************************************************
			 *  Search External.trigger("string", variable or { object });
			 *
			 *  Return:
			 *  - External call name
			 *  - parameters on json format, or variable name
			 *************************************************************************/
			var triggerExp = /External\.trigger\("([a-zA-Z0-9\\-]+)"(, ({[\n\t\r\v a-zA-Z:",]+}|.+))?\)/img,
				triggers = [],
				trigger,
				params;

			while ((trigger = triggerExp.exec(text)) !== null) {
				if (trigger.index === trigger.lastIndex) {
					trigger.lastIndex++;
				}

				if (trigger[3] && trigger[3].indexOf("{") > -1) {
					params = trigger[3].replace(/[\n\t\r\v]+/img, " ");
				}
				else
				{
					params = trigger[3];
				}

				if (!objectSearch(triggers, trigger[1])) {
					triggers.push({
						"name": trigger[1],
						"params": params?params:""
					});
				}
			}
			
			return triggers;
		},
		/**
		 * Fetch External binds on the document
		 * @param   {String} text Text contents of the javascript file to parse
		 * @returns {Array}  Array of objects. [{name:"", params:""}]
		 */
		fetchBinds: function (text) {
			/**************************************************************************
			 *  Search External.bind("string", variable or function() {} );
			 *
			 *  Return:
			 *  - External call name
			 *  - parameters of function, or variable name of function
			 *************************************************************************/
			var bindExp = /External\.bind\(\"([a-zA-Z0-9\-]+)\", (function\(([a-zA-Z, ]*)|[a-zA-Z_ ]+)\)/img,
				binds = [],
				bind;

			while ((bind = bindExp.exec(text)) !== null) {
				if (bind.index === bind.lastIndex) {
					bind.lastIndex++;
				}
				
				if (!objectSearch(binds, bind[1])) {
					binds.push({
						"name": bind[1],
						"params": ""
					});
				}
			}
			
			return binds;
		},
		/**
		 * Parses the text and stores the External calls
		 * @param 	{String} text Text contents
		 * @returns {Object} Object of triggers and binds
		 */
		parseText: function (text) {
			this.data = {
				triggers : this.fetchTriggers(text),
				binds : this.fetchBinds(text)
			};
			
			return this.data;
		},
		/**
		 * Create markdown string based on the object passed
		 * @param   {Object} object Object of triggers and binds
		 * @returns {String} Markdown format string
		 */
		createMarkDown : function (object) {
			var data = object || this.data,
				mdString = "",
				key = "";
			
			if (data.triggers.length > 0) {
				mdString += "# Triggers (JS_to_C)\n\n" +
							"<table>\n\t";
				
				for (key in data.triggers) {
					mdString += "\t<tr>\n\t\t";
					
					mdString += "<td>" + data.triggers[key].name + "</td>\n\t\t";
					mdString += "<td>" + data.triggers[key].params + "</td>\n\t";
					
					mdString+= "</tr>\n";
				}
				
				mdString += "</table>\n\n";
			}
			
			if (data.binds.length > 0) {
				mdString += "# Binds (C_to_JS)\n\n" +
							"<table>\n";
				
				for (key in data.binds) {
					mdString += "\t<tr>\n\t\t";
					
					mdString += "<td>" + data.binds[key].name + "</td>\n\t\t";
					mdString += "<td>" + data.binds[key].params + "</td>\n\t";
					
					mdString+= "</tr>\n";
				}
				
				mdString += "</table>\n\n";
			}
			
			return mdString;
		}
	};

	return FileParser;
})();

var fileParser = new FileParser();

exports.parseFile = function (text) {
	return fileParser.parseText(text);
};

exports.createMarkDown = function (object) {
	return fileParser.createMarkDown(object);
};